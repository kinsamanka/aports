# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.0.0_rc2
_ver=2.0.0-rc.2
pkgrel=1
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="compose-cli-$_ver.tar.gz::https://github.com/docker/compose-cli/archive/v$_ver.tar.gz"

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-cli-"$_ver"

export GOPATH=$srcdir/go
export GOCACHE=$srcdir/go-build
export GOTMPDIR=$srcdir

build() {
	PKG=github.com/docker/compose-cli
	local ldflags="-s -w -X $PKG/internal.Version=v$_ver"
	go build -modcacherw -ldflags "$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list -modcacherw ./... | grep -Ev '/e2e(/|$)')"
	go test -modcacherw -short $pkgs
	./docker-compose compose version
}

package() {
	install -Dm755 docker-compose "$pkgdir$_plugin_installdir"/docker-compose
}

sha512sums="
b65fe6d39139c8d802a783e14154b5ac0e8824836a94ef7628c989cff5ddae67610d3b600651db8dfb7243b1624f763058e9006a52e17e772ee50f3545aee389  compose-cli-2.0.0-rc.2.tar.gz
"
