# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=pueue
pkgver=1.0.1
pkgrel=0
pkgdesc="Manage your shell commands"
url="https://github.com/nukesor/pueue"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
arch="$arch !ppc64le" # Fails to build 'ring v0.16.19'
license="MIT"
checkdepends="bash"
makedepends="cargo"
source="https://github.com/Nukesor/pueue/archive/v$pkgver/$pkgname-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver"

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF
}

build() {
	cargo build --release --locked
}

check() {
	cargo test --locked
}

package() {
	cargo install --locked --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
1ea161be727c8a85b4e4ca5a04497e38b5e7fc1bc1c772223b4e5a531fa8495a3c7f6137610114bfc89c09e482a0cda713169c959205d433462dbd2303047be6  pueue-1.0.1.tar.gz
"
